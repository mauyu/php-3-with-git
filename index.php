<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    
    <h1>TUGAS PHP 3 WITH GITLAB</h1>


    <?php

        function enter(){
            echo "<br>";
        }

        echo "<h3> NOMER 1 </h3>";
        enter();

        function tentukan_nilai($number)
        {
            if($number >= 89){
                echo "SANGAT BAIK.. PERTAHANKAN KAU YANG TERBAIK = ";
            }
            elseif($number >= 75){ 
                echo "Baik.. KEMBANGKANN = ";
            }
            elseif($number >= 65){
                echo "Cukup.. BELAJARLAH LEBIH GIAT = ";
            }
            elseif($number >= 49){
                echo "Kurang.. TINGKATKAN BELAJARMU LAGI = ";
            }
            else{
                echo "Anda tinggal kelas/ tidak lulus karena mendapat nilai = ";
            };
            return $number;
        };

        //TEST CASES
        echo tentukan_nilai( 98); //Sangat Baik
        enter();
        echo tentukan_nilai( 76); //Baik
        enter();
        echo tentukan_nilai( 67); //Cukup
        enter();
        echo tentukan_nilai( 43); //Kurang
        enter();

        echo "<h3> NOMER 2 </h3>";
        enter();

        
        function ubah_huruf($str){
            $huruf = "abcdefghijklmnopqrstuvwxyz";
            $keluar = "";
            for($i = 0; $i < strlen($str); $i++){
                $posisi = strrpos($huruf, $str[$i]);
                $keluar .= substr($huruf, $posisi + 1, 1);
            }
            return $keluar;
        }
            
            // TEST CASES
            echo ubah_huruf('wow'); // xpx
            enter();
            echo ubah_huruf('developer'); // efwfmpqfs
            enter();
            echo ubah_huruf('laravel'); // mbsbwfm
            enter();
            echo ubah_huruf('keren'); // lfsfo
            enter();
            echo ubah_huruf('semangat'); // tfnbohbu
            enter();


        echo "<h3> NOMER 3 </h3>";

        
        function tukar_besar_kecil($string){
            $out = "";
            for($x = 0; $x < strlen($string); $x++){
                if (ctype_lower($string[$x])){
                    $out .= strtoupper($string[$x]);
                }else {
                    $out .= strtolower($string[$x]);
                }
            }
                return $out;
            }
            
            // TEST CASES
            echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
            enter();
            echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
            enter();
            echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
            enter();
            echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
            enter();
            echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"
            enter();

            
    ?>

</body>
</html>